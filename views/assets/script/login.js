const username = document.getElementById('username')
const password = document.getElementById('password')
const submit_button = document.getElementById('submit')
const alertPanel = document.getElementById('alert')

alertPanel.style.display = 'none'

username.addEventListener('click', () => {
    reset()
})
password.addEventListener('click', () => {
    reset()
})
submit_button.click();
password.addEventListener("keypress", function (event) {
    // If the user presses the "Enter" key on the keyboard
    if (event.key == "Enter") {
        console.log(event)
        // Cancel the default action, if needed
        // event.preventDefault();
        // Trigger the button element with a click
        // submit_button.click();
        do_login();
    }
});

function reset() {
    alertPanel.style.display = 'none'
}

submit_button.addEventListener('focus', () => {
    do_login()
    // fetch('http://localhost:300/login')
    //     .then(response => response.json())
    //     .then(data => console.log(data));

})

function do_login() {
    const _name = username.value
    const _pass = password.value

    if (!_name || !_pass) {
        alertPanel.style.display = 'block'
        return false
    }



    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    var urlencoded = new URLSearchParams();
    urlencoded.append("username", _name);
    urlencoded.append("password", _pass);

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    }

    fetch("http://localhost:3000/login", requestOptions)
        .then(response => response.json())
        .then(result => {
            console.log(result)
            if (result.success) {
                window.location.replace("http://localhost:3000/game")
            } else {
                alertPanel.style.display = 'block'
                password.value = ''
            }
        })
        .catch(error => console.log('error', error));
}