const user = require("../model/user.json")
const UserModel = require("../model/user.js")
const fs = require('fs')

class User {

  static getUser(req, res, next) {
    try {
      const _user = UserModel.getAllUser()
      res.status(200).json(_user)
    } catch (error) {
      next(error)
    }
  }
  static getOneUser(req, res, next) {
    try {
      const { username } = req.params
      const user = UserModel.getOneUser(username)
      res.status(200).json(user)
    } catch (error) {
      next(error)
    }
  }

  static loginPage(req, res, next) {
    res.render("login")
  }

  static login(req, res, next) {
    try {
      const { password, username } = req.body
      let idx = user.findIndex(x => x.username === username);
      console.log(idx, "<<< INDEX")
      if (idx >= 0) {

        const checkPassword = password == user[idx].password
        if (checkPassword == false) {
          throw {
            // status: 401,
            status: 200,
            success: false,
            message: "username or password is wrong",
          }
        } else {
          const _username = user[idx].username
          const _name = user[idx].name
          req.session.login = true;
          req.session.username = _username;
          req.session.name = _name;

          res.status(200).json({
            success: true,
            username: _username,
          })
        }
      } else {
        throw {
          // status: 401,
          status: 200,
          success: false,
          message: "username or password is wrong",
        }
      }
    } catch (error) {
      console.log(error)
      next(error)
    }
  }

  static logout(req, res, next) {
    req.session.login = false;
    req.session.username = null;
    req.session.name = null;
    res.redirect('/');
  }

  static registerPage(req, res, next) {
    res.render("register")
  }
  static register(req, res, next) {
    const { password, username, name } = req.body

    let idx = user.findIndex(x => x.username === username);
    if (idx >= 0) {
      res.status(200).json({
        success: false,
        message: "Username sudah digunakan",
      })
    } else {
      user.push({
        "username": username,
        "password": password,
        "name": name
      })

      res.status(200).json({
        success: true,
      })

    }


    // let data = this.fileToJson();
    // data[index][varName] = varValue;
    // fs.writeFileSync(this.filename, JSON.stringify(data))

  }
}


module.exports = User