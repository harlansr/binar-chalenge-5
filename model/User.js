const user = require("./user.json")
class UserModel {

    static getAllUser() {
        const _user = user

        _user.forEach(el => {
            delete el.password
        })

        return _user

    }

    static getOneUser(username) {
        const _user = user
        const idx = _user.findIndex(x => x.username == username)
        if (idx >= 0) {
            const finalData = _user[idx]
            delete finalData.password
            return finalData
        } else {
            return {
                error: true,
                message: "User not found",
            }
        }

    }
}

module.exports = UserModel