const route = require("express").Router()
const { Home, User } = require("../controller")
const Middleware = require("../middleware")


// Route
route.get("/", Home.home)
route.get("/login", Middleware.doWithoutAuth, User.loginPage)
route.post("/login", Middleware.doWithoutAuth, User.login)
route.get("/register", Middleware.doWithoutAuth, User.registerPage)
route.post("/register", Middleware.doWithoutAuth, User.register)
route.get("/logout", Middleware.doWithAuth, User.logout)
route.get("/game", Middleware.doWithAuth, Home.game)

// Just API
route.get("/user", User.getUser)
route.get("/user/:username", User.getOneUser)



module.exports = route